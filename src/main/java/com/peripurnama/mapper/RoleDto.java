package com.peripurnama.mapper;

public class RoleDto {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "RoleDto [name=" + name + "]";
	}

}