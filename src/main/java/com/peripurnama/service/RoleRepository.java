package com.peripurnama.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.peripurnama.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
